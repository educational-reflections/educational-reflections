# Educational Reflections

The repository uses the [GoLang structure](#go-project-layout) of the project and contains a submodule.
To clone a repository with a submodule, use the command `git clone --recurse-submodules https://gitlab.com/educational-reflections/educational-reflections.git`

## List of submodules:

[deployment folder with submodule ansible](https://gitlab.com/educational-reflections/ansible-reflections.git)
[build folder with submodule ci](https://gitlab.com/educational-reflections/ci-reflections.git)
[init folder with submodule configurations](https://gitlab.com/educational-reflections/configurations-reflections.git)

## GO project layout

```bash
.
├── /api                 # OpenAPI/Swagger specifications, JSON schema files, protocol definition files.
├── /assets              # Other resources necessary to use this repository (images, logos, etc.)
├── /build               # Assembly and Continuous Integration (CI). Contains configuration files and scripts for the cloud (AMI), container (Docker), packages (deb, rpm, pkg) in the /build/package directory. CI configuration files (travis, circle, drone) and scripts in the /build/ci directory.
│   └── /ci
│   └── /package
├── /cmd                 # The main applications for the current project. The name of the directory for each application must match the name of the executable file that is being built (for example, /cmd/myapp).
├── /configs             # Configuration file templates and default settings files.
├── /deployments         # Templates and configuration files for deploying IaaS, PaaS, system and container orchestration (docker-compose, kubernetes/helm, mesos, terraform, bosh).
│   └── ansible
├── /docs                # Wiki DevSecOps zero to hero.
├── /init                # Configuration files for system initialization (systemd, upstart, sysv) and process managers (runit, supervisord).
├── /internal            # Internal code of the application and libraries. This is code that should not be used in other applications and libraries. Additional structuring may be added to separate the open and closed parts of the internal code. The code of the application itself can be located in the /internal/app directory (for example, /internal/app/myapp), and the code that this application uses is in the /internal/pkg directory (for example, /internal/pkg/myprivlib).
├── README.md
└── /scripts             # Scripts for performing various assembly, installation, analysis, etc. operations.
```
